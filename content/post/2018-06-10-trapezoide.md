---
title: "Regla del Trapezoide"
date: 2018-06-09T23:42:45-04:00
draft: false
tags: ["math", "geogebra", "aproximación", "python"]
---



# Integración Numerica: Regla del Trapezoide

La regla del trapezoide es un método númerico que aproxima el valor de una integral definida. Si se considera la integral definida 
$$\int^a_b f(x)dx$$
Se asume que $f(x)$ es continua en $[a,b]$ y se divide el $[a,b]$ en $n$ intervalos de igual longitud
$$\Delta x =\frac{b-a}{n}$$
y se usan $n+1$ puntos.

$x_0=a$, $x_1=a+\Delta x$, $x_2=a+2\Delta x$, $\dots$, $x_n = a+n\Delta x=b$

Se computan los valores de $f(x)$ en esos puntos.

$y_0 = f(x_0)$, $y_1 = f(x_1)$, $y_2 = f(x_2)$, $\ldots$, $y_n = f(x_n)$

Se aproxima la integral usando $n$ trapezoides formados usando segmentos de lineas entre los puntos $(x\_{i-1},y\_{i-1})$ y $(x_i, y_i)$ para $1\le i\le n$.

Adicionando el área de $n$ trapecios se obtiene la aproximación

$$\int^a_bf(x)dx \approx \frac{(y_0+y_1)\Delta x}{2}+ \frac{(y_1+y_2)\Delta x}{2}+\frac{(y\_2+y\_3)\Delta x}{2}+\cdots +\frac{(y\_{n-1}+y_n)\Delta x}{2}$$

## Geogebra

<iframe scrolling="no" title="Regla del Trapecio" src="https://www.geogebra.org/material/iframe/id/WrVfGd53/width/1836/height/924/border/888888/smb/false/stb/false/stbh/false/ai/false/asb/false/sri/true/rc/false/ld/false/sdz/true/ctl/false" width="918px" height="462px" style="border:0px;"> </iframe>

## Función que aproxima la integral de una función usando la regla del trapezoide


```python
def trapezoidal(f, a, b, n):
    """
    Trapezoidal function.
 
    Esta funcion computa la aproximacion de la integral definida de una funcion.
 
    Parameters:
    arg1 (lambda function): Funcion.
    arg2 (float, int): Limite inferior del intervalo de integracion.
    arg3 (float, int): Limite superior del intervalo de integracion.
    arg4 (int): Determina cuantas partes se divide en intervalo.
 
    Returns:
    float: Aproximación de la integral definida de la función sobre el intervalo [a,b] 
 
    """
    h = float(b-a)/n
    resultado = 0.5*f(a) + 0.5*f(b)
    for i in range(1, n):
        resultado += f(a + i*h)
    resultado *= h
    return resultado
```


### Prueba de nuestra función

Como función usaremos $f(x) = 3x^2e^{x^3}$, para calcular la integral en el intervalo $[0,1]$. Además calulemos calculemos el error si lo comparamos con el valor exacto, es decir, si aplicamos el Teorema Fundamental del Cálculo
a la función $F(x) = e^{x^3}$, sobre $[0,1]$


```python
from math import exp
f = lambda x: 3*(x**2)*exp(x**3)
n = 4
a, b = 0, 1
aproximacion = trapezoidal(f, a, b, n)
print "La integral aproximadad de f es {}".format(aproximacion)

```

    La integral aproximadad de f es 1.92271675047



### Computemos el error


```python
F = lambda x: exp(x**3)
exacta = F(1) - F(0)
print "El valor de el error es {}".format(exacta - aproximacion)
```

    El valor de el error es -0.204434922009

